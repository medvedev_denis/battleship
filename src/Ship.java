public class Ship {

    private int battlefield[] = new int[10];
    private boolean shipDestroy = false;
    private int tryShot = 0;
    private String hitString = "";

    public Ship(String userString){
        int shipCoordinate = Integer.parseInt(userString);
        for (int i = 0;i<3;i++) {
            battlefield[shipCoordinate] = 1;
            shipCoordinate++;
        }
        System.out.println("Корабль появился!");
    }

    public String shipShoot(int shot){
        tryShot++;
        if(battlefield[shot] == 1){
            battlefield[shot] = 0;
            for (int i = 0;i<battlefield.length;i++){
                if (battlefield[i] == 0){
                    hitString = "Уничтожил!";
                } else {
                    hitString = "Попадание!";
                    break;
                }
            }
        } else {
            hitString = "Мимо";
        }
        return hitString;
    }

    public boolean shipDestroy(String hitString){
        if (hitString.equals("Уничтожил!")) {
            return true;
        } else {
            return false;
        }
    }

    public String getHitString() {
        return hitString;
    }

    @Override
    public String toString() {
        return "Игра завершена! Выстрелов до уничтожения корабля: " + tryShot;
    }
}