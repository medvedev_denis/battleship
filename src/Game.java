import java.util.Scanner;
import java.util.Random;

public class Game {

    static Scanner sc = new Scanner(System.in);
    static Random random = new Random();

    public static void main(String[] args) {
        System.out.printf("Правила игры: \n 1.Вы можете стрелять по координатам от 0 до 9; \n 2.Вы можете стрелять по уже введенным ранее координатам. \n Приятной игры \n");
        String shipCoordinate = "" + random.nextInt(7);
        Ship ship = new Ship(shipCoordinate);
        shipShooting(ship);
        System.out.println(ship.toString());
    }

    public static void shipShooting(Ship ship){
        int shot;
        while (!ship.shipDestroy(ship.getHitString())) {
            System.out.println("Введите координату выстрела");
            shot = sc.nextInt();
            String shipShootResult = ship.shipShoot(shot);
            System.out.println(shipShootResult);
        }
    }
}